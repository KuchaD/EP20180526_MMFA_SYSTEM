﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using EP20180526_MMFA_SYSTEM.Models;

namespace EP20180526_MMFA_SYSTEM
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            var connection = "Server=DESKTOP-E81O8UV;Database=MMFA;Trusted_Connection=True;ConnectRetryCount=0";// Configuration["DefaultConnection"].ToString() ;
            services.AddDbContext<Models.DB.MMFAContext>(options => options.UseSqlServer(connection));
            services.AddSession();

            services.AddIdentity<ApplicationUser, IdentityRole>(options => {
                options.Lockout.AllowedForNewUsers = true;
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                
            })
           .AddEntityFrameworkStores<Models.DB.MMFAContext>()
           .AddDefaultTokenProviders();

            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.Expiration = TimeSpan.FromSeconds(60);
                options.LoginPath = "/Account/Login";
              
            });
            //services.AddTransient<IUserStore<Models.DB.Users>, MyUserStore<Models.DB.Users>>();
            //services.AddTransient<IRoleStore<Models.DB.Role>, MyRoleStore<Models.DB.Role>>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                app.UseSession();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}

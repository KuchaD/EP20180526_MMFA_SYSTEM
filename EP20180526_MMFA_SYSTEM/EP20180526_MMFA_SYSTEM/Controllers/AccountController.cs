﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace EP20180526_MMFA_SYSTEM.Controllers
{


    public class AccountController : Controller
    {
        private readonly UserManager<Models.ApplicationUser> _userManager;
        private readonly SignInManager<Models.ApplicationUser> _signInManager;
        private readonly Models.DB.MMFAContext _Context;
        private readonly RoleManager<IdentityRole> _roleManager;

        public AccountController(
            UserManager<Models.ApplicationUser> userManager,
            SignInManager<Models.ApplicationUser> signInManager,
            RoleManager<IdentityRole> roleManager,
            Models.DB.MMFAContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _Context = context;
            _roleManager = roleManager;
        }

        public async Task<IActionResult> Registration()

        {            
            return View(); }
        [HttpPost]
        public async Task<IActionResult> Registration(Models.UserRegistration aModel)
        {
            if (ModelState.IsValid || aModel.Permission == true)
            {

                Models.ApplicationUser lNewUser = new Models.ApplicationUser { UserName = aModel.Email, Email = aModel.Email, PasswordHash = aModel.Password };

                var result = await _userManager.CreateAsync(lNewUser, aModel.Password);

                if (result.Succeeded)
                {
                    //await _signInManager.SignInAsync(lNewUser, false);


                    //await _userManager.AddToRoleAsync(lNewUser, "employers");
                    await _userManager.AddToRoleAsync(lNewUser, "First");
                    //stranka pro doplneni info
                    ViewBag.Error = "Byl jsi uspěšně regitrován.Pro přístup do systemu MMFA musíš být autorizován o autorizování ti přijde informační email. Pokud nastal problem kontaktuj admin@admin.cz";
                    return View("Login");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                }
            }
            return View(aModel);


        }
        public async Task<IActionResult> Login() {
            if (_signInManager.IsSignedIn(User))
                return RedirectToAction("Index","Home");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Login(Models.UserLog aModel)
        {

            if (ModelState.IsValid)
            {
                if (_Context.ApplicationUser.Single(aR => aR.UserName == aModel.Email)._Authorization == false)
                {
                    ViewBag.Error = "Nejsi autorizován pro přístup do IS MMFA. Prosím kontaktuj správce admin@admin.cz";

                    return View();
                }
                Models.ApplicationUser lNewUser = new Models.ApplicationUser { UserName = aModel.Email, Email = aModel.Email, PasswordHash = aModel.Password };

                if (ModelState.IsValid)
                {
                    var result = await _signInManager.PasswordSignInAsync(lNewUser.UserName,
                       lNewUser.PasswordHash, aModel.RememberMe, false);

                    if (result.Succeeded)
                    {
                        //stranka pro dopleni informaci

                        if (User.IsInRole("First"))
                            return RedirectToAction("Index", "Information");
                        else
                            return RedirectToAction("Index", "Home");
                    }
                }
            }
            
            return View(aModel);
        }

        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return Redirect("/account/login");
        }

    }
}
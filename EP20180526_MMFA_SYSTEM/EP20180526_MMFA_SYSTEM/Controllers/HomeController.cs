﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EP20180526_MMFA_SYSTEM.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace EP20180526_MMFA_SYSTEM.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly UserManager<Models.ApplicationUser> _userManager;
        private readonly SignInManager<Models.ApplicationUser> _signInManager;
        private readonly Models.DB.MMFAContext _Context;
        private readonly RoleManager<IdentityRole> _roleManager;

        public HomeController(
            UserManager<Models.ApplicationUser> userManager,
            SignInManager<Models.ApplicationUser> signInManager,
            RoleManager<IdentityRole> roleManager,
            Models.DB.MMFAContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _Context = context;
            _roleManager = roleManager;
        }
        
        public IActionResult Index()
        {
            if (User.IsInRole("First"))
                return RedirectToAction("Index","Information");
            else
                return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

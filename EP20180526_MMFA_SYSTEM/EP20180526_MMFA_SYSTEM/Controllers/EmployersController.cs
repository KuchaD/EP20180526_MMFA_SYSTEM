﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace EP20180526_MMFA_SYSTEM.Controllers
{
    [Authorize(Roles = "Admin")]
    public class EmployersController : Controller
    {
        private readonly UserManager<Models.ApplicationUser> _userManager;
        private readonly SignInManager<Models.ApplicationUser> _signInManager;
        private readonly Models.DB.MMFAContext _Context;
        private readonly RoleManager<IdentityRole> _roleManager;

        public EmployersController(
            UserManager<Models.ApplicationUser> userManager,
            SignInManager<Models.ApplicationUser> signInManager,
            RoleManager<IdentityRole> roleManager,
            Models.DB.MMFAContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _Context = context;
            _roleManager = roleManager;
        }
        public IActionResult Index()
        {
            
            return View(_Context.ApplicationUser.ToList());
        }
        public IActionResult acceptNoAuthorizeAll()
        {
            var lUsers = _Context.ApplicationUser.Where(aR => aR._Authorization == false);
            foreach(var nUser in lUsers)
                nUser._Authorization = true;
            _Context.SaveChanges();
            return RedirectToAction("NoAuthorizeEmpoyers");
        }
        public IActionResult NoAuthorizeEmpoyers()
        {
            return View(_Context.ApplicationUser.Where(aR => aR._Authorization == false).ToList());
        }
        [HttpPost]
        public IActionResult NoAuthorizeEmpoyers(IEnumerable<Models.ApplicationUser> aModels)
        {
            return View();
        }

        [HttpGet]
        public IActionResult NoAuthorizeEmpoyersUserName(string id)
        {
            return View(_Context.ApplicationUser.Where(aR => aR._Authorization == false && aR.UserName == id).ToList());
            
        }



        public IActionResult Authorize(string id)
        {
            var lUser = _Context.ApplicationUser.FirstOrDefault(aR => aR.Id == id);
            lUser._Authorization = true;
             _Context.SaveChanges();
            return RedirectToAction("NoAuthorizeEmpoyers");
        }

        public async Task<IActionResult> UnAuthorize(string id)
        {
            var lUser = _Context.ApplicationUser.FirstOrDefault(aR => aR.Id == id);
            lUser._Authorization = false;
            await _Context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Delete(string id)
        {
            var lUser = _Context. ApplicationUser.FirstOrDefault(aR => aR.Id == id);
            _Context.Remove(lUser);
            await _Context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public IActionResult Create()
        {
            return View();
        }


        }
    }
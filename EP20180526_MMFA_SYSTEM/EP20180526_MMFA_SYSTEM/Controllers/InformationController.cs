﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace EP20180526_MMFA_SYSTEM.Controllers
{
    [Authorize(Roles = "First")]
    public class InformationController : Controller
    {
        private readonly UserManager<Models.ApplicationUser> _userManager;
        private readonly SignInManager<Models.ApplicationUser> _signInManager;
        private readonly Models.DB.MMFAContext _Context;
        private readonly RoleManager<IdentityRole> _roleManager;

        public InformationController(
            UserManager<Models.ApplicationUser> userManager,
            SignInManager<Models.ApplicationUser> signInManager,
            RoleManager<IdentityRole> roleManager,
            Models.DB.MMFAContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _Context = context;
            _roleManager = roleManager;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult>Index(Models.ContollersModel.InformationModel aModel)
        {
            if (ModelState.IsValid && aModel._Permission == true)
            {
                var lActualUser = await _userManager.GetUserAsync(User);
                await _userManager.RemoveFromRoleAsync(lActualUser, "First");
                await _userManager.AddToRoleAsync(lActualUser, "Employers");

                var lUpdateUser = _Context.ApplicationUser.FirstOrDefault(aR => aR.Email == lActualUser.Email);
                lUpdateUser.Name = aModel.Name;
                lUpdateUser.Surname = aModel.Surname;
                lUpdateUser.Steet = aModel.Steet;
                lUpdateUser.City = aModel.City;
                lUpdateUser.Birthdate = aModel.Birthdate;
                lUpdateUser.Birthnumber = aModel.Birthnumber;
                lUpdateUser.BankAccount = aModel.BankAccount;
                lUpdateUser.PhoneNumber = aModel.Phone;
                lUpdateUser.SaleOnTax = aModel.SaleOnTax;
                lUpdateUser._Permission = aModel._Permission;
                lUpdateUser.Insurence = aModel.Insurence;

                _Context.SaveChanges();
               return Redirect("Account/Logout");
            }

            return View();
        }
    }
}
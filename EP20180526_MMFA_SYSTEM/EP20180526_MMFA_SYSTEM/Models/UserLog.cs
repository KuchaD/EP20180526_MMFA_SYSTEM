﻿using EP20180526_MMFA_SYSTEM.Models.DB;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace EP20180526_MMFA_SYSTEM.Models
{
    public class UserLog
    {

        [Required]
        public string Email { get; set; }
        [DisplayName("Heslo")]
        [Required]
        public string Password { get; set; }
        [DisplayName("Zapamatovat")]
        public bool RememberMe { get; set; }
        
    }
    
}


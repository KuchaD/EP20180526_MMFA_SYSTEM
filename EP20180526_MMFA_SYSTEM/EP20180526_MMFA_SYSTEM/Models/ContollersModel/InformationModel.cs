﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EP20180526_MMFA_SYSTEM.Models.ContollersModel
{
    public class InformationModel
    {
        [Required]
        [DisplayName("Jméno")]
        public string Name { get; set; }
        [Required]
        [DisplayName("Příjmení")]
        public string Surname { get; set; }
        [Required]
        [DisplayName("Datum narození")]
        [DataType(DataType.Date)]
        public DateTime Birthdate { get; set; }
        [Required]
        [RegularExpression(@"^[0-9]{6}\/[0-9]{4}^\b")]
        [DisplayName("Rodné číslo")]
        public string Birthnumber { get; set; }
        [Required]
        [DisplayName("Adresa")]
        public string Steet { get; set; }
        [Required]
        [DisplayName("Město")]
        public string City { get; set; }
        [Required]
        [RegularExpression(@"^[0-9]*\-?[0-9]+\/[0-9]{4}\b")]
        [DisplayName("Bankovní účet")]
        public string BankAccount { get; set; }
        
        [DisplayName("Sleva na dani")]
        public bool SaleOnTax { get; set; }

        [Required]
        [DisplayName("Pojištovna")]
        [Range(0,999)]
        public int Insurence { get; set; }
        [Required]
        [DisplayName("Telefoní číslo")]
        [Phone]
        public string Phone { get; set; }
        [Required]
        [DisplayName("Souhlasím")]
        public bool _Permission { get; set; }


    }
}

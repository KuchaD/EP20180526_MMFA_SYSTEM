﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EP20180526_MMFA_SYSTEM.Models.DAL
{
    public class WorkRole
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public double ExtraSalary_h { get; set; }
        public double ExtraPremie { get; set; }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using EP20180526_MMFA_SYSTEM.Models;
using Microsoft.Extensions.Configuration;

namespace EP20180526_MMFA_SYSTEM.Models.DB
{
    public partial class MMFAContext : IdentityDbContext<ApplicationUser> { 



        public MMFAContext(DbContextOptions<MMFAContext> options)
: base(options)
        { }
        public MMFAContext() { }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                
                
                //    optionsBuilder.UseSqlServer("server = free.aspify.com; uid = db537; pwd = Kiklop32; database = db537");
                optionsBuilder.UseSqlServer("Server=DESKTOP-E81O8UV;Database=MMFA;Trusted_Connection=True;ConnectRetryCount=0");
            }
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
        }


        public DbSet<EP20180526_MMFA_SYSTEM.Models.ApplicationUser> ApplicationUser { get; set; }
        public DbSet<EP20180526_MMFA_SYSTEM.Models.DAL.Attendance> Attendance { get; set; }
        public DbSet<EP20180526_MMFA_SYSTEM.Models.DAL.DateShip> DateShip { get; set; }
        public DbSet<EP20180526_MMFA_SYSTEM.Models.DAL.PossibilityWorkers> PossibilityWorkers { get; set; }
        public DbSet<EP20180526_MMFA_SYSTEM.Models.DAL.Ship> Ship { get; set; }
        public DbSet<EP20180526_MMFA_SYSTEM.Models.DAL.WorkRole> WorkRole { get; set; }
    }
}

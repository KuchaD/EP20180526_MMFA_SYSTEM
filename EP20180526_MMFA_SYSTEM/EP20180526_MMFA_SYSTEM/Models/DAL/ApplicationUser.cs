﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EP20180526_MMFA_SYSTEM.Models
{
    public class ApplicationUser : IdentityUser
    {
        public DateTime _LastLogin { get; set; }
        public bool _Authorization { get; set; }
        public bool _Permission { get; set; }

        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime Birthdate { get; set; }
        public string Birthnumber { get; set; }
        public string Steet { get; set; }
        public string City { get; set; }
        public string BankAccount { get; set; }
        public bool SaleOnTax { get; set; }
        public byte[] Avatar { get; set; }
        public int Apologize { get; set; }
        public int Insurence { get; set; }
        public byte[] Contract { get; set; }
        public DateTime EndOfContract { get; set; }

    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EP20180526_MMFA_SYSTEM.Models.DAL
{
    public class Attendance
    {
        public Guid ID { get; set; }
        public ApplicationUser Worker { get; set; }
        public WorkRole Role { get; set; }
        public DateTime LogOut { get; set; }
        public TimeSpan Break { get; set; }
        public double Salary_h { get; set; }
        public double Premie { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}

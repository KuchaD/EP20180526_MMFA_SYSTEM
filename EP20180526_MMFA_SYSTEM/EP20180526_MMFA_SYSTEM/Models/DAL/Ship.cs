﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EP20180526_MMFA_SYSTEM.Models.DAL
{
    public class Ship
    {
        public Guid ID { get; set; }
        public DateTime Date { get; set; }
        public DateTime DeadLine { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public double Rate_czk { get; set; }
        public string Notes { get; set; }
        public int Capacity { get; set; }
        public DateTime ExpectedStart { get; set; }
        public DateTime ExpextedEnd { get; set; }
        public string MeetingPoint { get; set; }
        public List<Attendance> Workers { get; set; }
    }
}

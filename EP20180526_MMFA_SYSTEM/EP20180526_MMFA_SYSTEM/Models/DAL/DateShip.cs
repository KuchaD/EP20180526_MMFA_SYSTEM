﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EP20180526_MMFA_SYSTEM.Models.DAL
{
    public class DateShip
    {
        public Guid ID { get; set; }
        public DateTime Date{ get; set; }

        public List<Ship> Ships { get; set; }
        public List<PossibilityWorkers> PossibilityWorkers { get; set; }
    }
}

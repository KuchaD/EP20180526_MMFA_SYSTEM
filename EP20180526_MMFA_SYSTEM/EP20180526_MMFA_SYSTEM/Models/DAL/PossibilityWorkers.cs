﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EP20180526_MMFA_SYSTEM.Models.DAL
{
    public class PossibilityWorkers
    {
        public bool _Active { get; set; }

        public Guid ID { get; set; }
        public DateTime LogOut { get; set; }
        public DateTime LogOn { get; set; }
        public ApplicationUser Worker { get; set; }
        public string Notes { get; set; }
    }
}

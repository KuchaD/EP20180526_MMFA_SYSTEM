﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EP20180526_MMFA_SYSTEM.Models
{
    public class UserRegistration
    {
        [Required]
        public string Email {get;set ;}
        [Required]
        [DisplayName("Heslo")]
        public string Password { get; set; }
        [Required]
        [Compare("Password")]
        [DisplayName("Znovu heslo")]
        public string Password2 { get; set; }
        [Required]
        [DisplayName("Souhlasím s podmínkami")]
        public bool Permission { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace EP20180526_MMFA_SYSTEM.Migrations
{
    public partial class AddProductReviews : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DateShip",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DateShip", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "WorkRole",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    ExtraPremie = table.Column<double>(nullable: false),
                    ExtraSalary_h = table.Column<double>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkRole", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "PossibilityWorkers",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    DateShipID = table.Column<Guid>(nullable: true),
                    LogOn = table.Column<DateTime>(nullable: false),
                    LogOut = table.Column<DateTime>(nullable: false),
                    Notes = table.Column<string>(nullable: true),
                    WorkerId = table.Column<string>(nullable: true),
                    _Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PossibilityWorkers", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PossibilityWorkers_DateShip_DateShipID",
                        column: x => x.DateShipID,
                        principalTable: "DateShip",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PossibilityWorkers_AspNetUsers_WorkerId",
                        column: x => x.WorkerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Ship",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    City = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    DateShipID = table.Column<Guid>(nullable: true),
                    DeadLine = table.Column<DateTime>(nullable: false),
                    ExpectedStart = table.Column<DateTime>(nullable: false),
                    ExpextedEnd = table.Column<DateTime>(nullable: false),
                    MeetingPoint = table.Column<string>(nullable: true),
                    Notes = table.Column<string>(nullable: true),
                    Rate_czk = table.Column<double>(nullable: false),
                    Street = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ship", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Ship_DateShip_DateShipID",
                        column: x => x.DateShipID,
                        principalTable: "DateShip",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Attendance",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Break = table.Column<TimeSpan>(nullable: false),
                    End = table.Column<DateTime>(nullable: false),
                    LogOut = table.Column<DateTime>(nullable: false),
                    Premie = table.Column<double>(nullable: false),
                    RoleID = table.Column<Guid>(nullable: true),
                    Salary_h = table.Column<double>(nullable: false),
                    ShipID = table.Column<Guid>(nullable: true),
                    Start = table.Column<DateTime>(nullable: false),
                    WorkerId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Attendance", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Attendance_WorkRole_RoleID",
                        column: x => x.RoleID,
                        principalTable: "WorkRole",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Attendance_Ship_ShipID",
                        column: x => x.ShipID,
                        principalTable: "Ship",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Attendance_AspNetUsers_WorkerId",
                        column: x => x.WorkerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Attendance_RoleID",
                table: "Attendance",
                column: "RoleID");

            migrationBuilder.CreateIndex(
                name: "IX_Attendance_ShipID",
                table: "Attendance",
                column: "ShipID");

            migrationBuilder.CreateIndex(
                name: "IX_Attendance_WorkerId",
                table: "Attendance",
                column: "WorkerId");

            migrationBuilder.CreateIndex(
                name: "IX_PossibilityWorkers_DateShipID",
                table: "PossibilityWorkers",
                column: "DateShipID");

            migrationBuilder.CreateIndex(
                name: "IX_PossibilityWorkers_WorkerId",
                table: "PossibilityWorkers",
                column: "WorkerId");

            migrationBuilder.CreateIndex(
                name: "IX_Ship_DateShipID",
                table: "Ship",
                column: "DateShipID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Attendance");

            migrationBuilder.DropTable(
                name: "PossibilityWorkers");

            migrationBuilder.DropTable(
                name: "WorkRole");

            migrationBuilder.DropTable(
                name: "Ship");

            migrationBuilder.DropTable(
                name: "DateShip");
        }
    }
}
